﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChartButtonManager : MonoBehaviour
{
    public GameObject verticalBarChartButton, horizontalBarChartButton, pieChartButton;

    public GameObject verticalBC, horizontalBC, pieChart;

    public DataManager verDataManager, horDataManager;

    public PieChartManager pieDataManager;

    public float[] tempValues;
    
    public void VerticalBarChartButtonPress()
    {
        if(pieChart.activeInHierarchy)
        {
            for(int i = 0; i < tempValues.Length; i++)
            {
                tempValues[i] = pieDataManager.values[i]; 
            }
        }
        else
        {
            for(int i = 0; i < tempValues.Length; i++)
            {
                tempValues[i] = horDataManager.verticalBarList[i].valueBar; 
            }
        }
        verticalBC.SetActive(true);
        horizontalBC.SetActive(false);
        pieChart.SetActive(false);

        StartCoroutine(ChangeDelay(true));

    }

    IEnumerator ChangeDelay(bool toVer)
    {   Debug.Log(toVer);
        yield return new WaitForSeconds(0.001f);
        if(toVer)
        {
            for(int i = 0; i < tempValues.Length; i++)
            {
                verDataManager.verticalBarList[i].valueBar = tempValues[i];
            }
        }   
        else
        {
            for(int i = 0; i < tempValues.Length; i++)
            {
                horDataManager.verticalBarList[i].valueBar = tempValues[i];
            }
        }
        

    }

    public void HorizontalBarChartButtonPress()
    {
        if(pieChart.activeInHierarchy)
        {
            for(int i = 0; i < tempValues.Length; i++)
            {
                tempValues[i] = pieDataManager.values[i]; 
            }
        }
        else
        {
            for(int i = 0; i < tempValues.Length; i++)
            {
                tempValues[i] = horDataManager.verticalBarList[i].valueBar; 
            }
        }

        horizontalBC.SetActive(true);
        verticalBC.SetActive(false);
        pieChart.SetActive(false);
        StartCoroutine(ChangeDelay(false));
    }
    

    public void PieChartButtonPress()
    {
        pieChart.SetActive(true);
        verticalBC.SetActive(false);
        horizontalBC.SetActive(false);
    }
    
}
