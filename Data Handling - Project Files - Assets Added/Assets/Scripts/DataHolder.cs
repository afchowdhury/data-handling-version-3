﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHolder : MonoBehaviour
{

    public List<int> numberData = new List<int>();
    public List<string> stringData = new List<string>();
 
}
