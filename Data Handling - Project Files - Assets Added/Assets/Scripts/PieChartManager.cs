﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PieChartManager : MonoBehaviour
{
    public Image[] pieChartPieces;

    public List<InputField> rightTextList = new List<InputField>();

    public List<InputField> titleTextList = new List<InputField>();
    public List<Text> percentageTexts = new List<Text>();
    public float percResult;

    public float[] values;

    public DataHolder dataH;

    public Text scoreText;

    int score = 0;

    int total;
    
    //public List<float> values = new List<float>();

    public void SetValues(float[] valueToSet)
    {
        float totalValues = 0;
        for(int i = 0; i < pieChartPieces.Length; i++)
        {
            totalValues += FindPercentage(valueToSet, i);
            pieChartPieces[i].fillAmount = totalValues;
            if(values[i] == 0)
            {
                pieChartPieces[i].gameObject.SetActive(false);
            }
            else
            {
                pieChartPieces[i].gameObject.SetActive(true);
            }
        }
        TotalScoreDisplay();
        RightTextChange();
        DisplayPercentage();

    }


     public void PullData (){
        
        PieChartChange();
        TitleTextChange();

     }

    public void RightTextChange()
    {
        for(int i = 0; i < rightTextList.Count ; i++)
        {
            Debug.Log(values[i].ToString());
            rightTextList[i].text = values[i].ToString();

        }
        
    }

    float FindPercentage(float[] valueToSet, int index)
    {
        float totalAmount = 0;
        for(int i = 0; i < valueToSet.Length; i++)
        {
            totalAmount += valueToSet[i];
        }
        return valueToSet[index] / totalAmount;
    }


    // Start is called before the first frame update
    void Start()
    {
        //SetValues(values);
    }

    // Update is called once per frame
    void Update()
    {
  
    }

    public void PieChartChange()
    {
        for(int i = 0; i < pieChartPieces.Length ; i++)
        {
            
            //values[i] = float.Parse(rightTextList[i].text);
            //dataH.numberData[i] = Mathf.RoundToInt(values[i]);

            values[i] = dataH.numberData[i];
        }
        SetValues(values);
    }

    
        public void AddButton(int pieSlice)
    {
        

       values[pieSlice]++;

       SetValues(values);
    }

    public void MinusButton(int pieSlice)
    {
        if(values[pieSlice] > 0)
        {
            values[pieSlice]--;
        }
        
        SetValues(values);
    }


    public void TitleTextChange()
    {

        for(int k = 0; k < titleTextList.Count; k++)
        {
            titleTextList[k].text = dataH.stringData[k];
        }
    }


    public void TotalScoreDisplay()
    {
        total = 0;
        foreach (var item in values)
        {

            total += Mathf.RoundToInt(item);
            scoreText.text = "Total: " + Mathf.RoundToInt(total).ToString();
        }

        
    }

    public void DisplayPercentage()
    {
        for(int i = 0; i < values.Length; i++)
        {
            if(values[i] == 0)
            {
                percentageTexts[i].text = "0%";
            }
            else
            {
                percResult = (values[i]/total)*100f;
                percentageTexts[i].text = percResult.ToString("0.0") + "%";
            }
        }   
    }
}


