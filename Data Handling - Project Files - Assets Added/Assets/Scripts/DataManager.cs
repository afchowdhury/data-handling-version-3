using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataManager : MonoBehaviour
{
    public List<InputField> rightTextList = new List<InputField>();

    public List<InputField> titleTextList = new List<InputField>();

    public List<VerticalBarChartManager> verticalBarList = new List<VerticalBarChartManager>();

    public VerticalBarChartManager verticalBarChartManager;

    public DataHolder dataH;

    //public List<int> numberList = new List<int>();

    public float number2, topNumber;
    public Text textNumber2, textNumber3, textNumber4, textNumber5, textTopNumber;
    
    // Start is called before the first frame update
    void Start()
    {
        //BarChartChange();
        RightTextChange();
        NumberCalculator();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PullData (){
        for(int i = 0; i < verticalBarList.Count ; i++){
            verticalBarList[i].valueBar = dataH.numberData[i];
        }
        BarChartChange();
        RightTextChange();
        TitleTextChange();
    }

    public void BarChartChange()
    {
        for(int i = 0; i < verticalBarList.Count ; i++)
        {
            
            verticalBarList[i].valueBar = dataH.numberData[i];
            if(dataH.numberData[i] > topNumber)
            {
                topNumber = dataH.numberData[i];
                NumberCalculator();

            }

            dataH.numberData[i] = Mathf.RoundToInt(verticalBarList[i].valueBar);

        }

        ChangeBarMax();

    }

    public void ChangeBarMax()
    {
        foreach (VerticalBarChartManager barChart in verticalBarList)
        {
            if(topNumber > 5){
            barChart.valueSlider.maxValue = topNumber;

            }
            else{
                barChart.valueSlider.maxValue = 5;
            }
        }
    }

    public void RightTextChange()
    {
        for(int i = 0; i < rightTextList.Count ; i++)
        {
            rightTextList[i].text = verticalBarList[i].valueBar.ToString();

        }
        
    }

    public void TitleTextChange()
    {

        for(int k = 0; k < titleTextList.Count; k++)
        {
            titleTextList[k].text = dataH.stringData[k];
        }
    }

    public void NumberCalculator()
    {
        
        number2 = (topNumber / 10) * 2;

        if(topNumber >= 5)
        {
            //Mathf.RoundToInt(number2);
            textNumber2.text = number2.ToString("F0");
            textNumber3.text = (number2 * 2).ToString("F0");
            textNumber4.text = (number2 * 3).ToString("F0");
            textNumber5.text = (number2 * 4).ToString("F0");
            textTopNumber.text = topNumber.ToString("F0");
        }
        else
        {
            textNumber2.text = number2.ToString();
            textNumber3.text = (number2 * 2).ToString();
            textNumber4.text = (number2 * 3).ToString();
            textNumber5.text = (number2 * 4).ToString();
            textTopNumber.text = topNumber.ToString();
            
        }

     
    }
}
