﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerticalBarChartManager : MonoBehaviour
{
    public float valueBar;

    public Slider valueSlider;

    public int maxValue = 10;

    public DataManager dataManager;

    public int topNumber;
    
    // Start is called before the first frame update
    void Start()
    {

        //valueBar = Mathf.Clamp(valueBar, 0, maxValue);
    }

    // Update is called once per frame
    void Update()
    {
        valueSlider.value = valueBar;
        
        //valueBar = (Mathf.clamp (0, maxValue)); 
        
    }

    public void AddButton()
    {
        valueBar++;

        if(valueBar > valueSlider.maxValue)
        {
            dataManager.topNumber = valueBar;
            dataManager.NumberCalculator();
            
            //valueSlider.maxValue = valueBar;
            foreach (VerticalBarChartManager barChart in dataManager.verticalBarList)
            {
                barChart.valueSlider.maxValue = valueBar;

            }

        }

    }

    public void MinusButton()
    {
        if(valueSlider.value > 0)
        {
            valueBar--;
        }
        

    }
}
