﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChartSelector : MonoBehaviour
{
    // 0 vertical 1 horizontal 2 pie


    public int currentChart;
    //public int nextChart;

    public GameObject[] chartObjs;
    
    public DataManager verDataManager, horDataManager;

    public PieChartManager pieDataManager;


    public DataHolder dataH;
    // Start is called before the first frame update
    void Start()
    {
        ChangeObject(0);
    }

    public void ChangeObject(int nextNum){


        switch (currentChart)
        {
        case 2:
                for (int i = 0; i < dataH.numberData.Count; i++)
            {
                dataH.numberData[i] = Mathf.RoundToInt(pieDataManager.values[i]);
                dataH.stringData[i] = pieDataManager.titleTextList[i].text;
            }
            break;
        case 1:
                for (int i = 0; i < dataH.numberData.Count; i++)
            {
                dataH.numberData[i] = Mathf.RoundToInt(horDataManager.verticalBarList[i].valueBar);
                dataH.stringData[i] = horDataManager.titleTextList[i].text;
            }
            
            break;
        case 0:
                for (int i = 0; i < dataH.numberData.Count; i++)
            {
                dataH.numberData[i] = Mathf.RoundToInt(verDataManager.verticalBarList[i].valueBar);
                dataH.stringData[i] = verDataManager.titleTextList[i].text;
            }

            break;

        default:
            print ("Broken");
            break;
        }

        currentChart = nextNum;

        switch (currentChart)
        {
        case 2:
            foreach (var item in chartObjs)
            {
                item.SetActive(false);
            }
                chartObjs[2].SetActive(true);
 
            Debug.Log("Fill Data");
            pieDataManager.PullData();
            break;
        case 1:
            foreach (var item in chartObjs)
            {
                item.SetActive(false);
            }
            chartObjs[1].SetActive(true);

            Debug.Log("Fill Data");
            horDataManager.PullData();

            break;
        case 0:
            foreach (var item in chartObjs)
            {
                item.SetActive(false);
            }
                chartObjs[0].SetActive(true);
       
            Debug.Log("Fill Data");
            verDataManager.PullData();
            break;

        default:
            print ("Broken");
            break;
        }



    }
}
