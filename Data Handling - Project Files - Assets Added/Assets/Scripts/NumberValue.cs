﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NumberValue : MonoBehaviour
{

    public TMP_Text myText;
    public int myNum;
    public float myMulti;
    public float passedInt;

    public GameManager gameMan;

    public void ChangeNum(bool up)
    {
        
        if (up)
        {
            myNum++;
        }
        else
        {
            myNum--;
        }

        myNum = Mathf.Clamp(myNum, 0, 9);

        UpdateNum();

        
    }

    public void UpdateNum()
    {
        myText.text = myNum.ToString();
        passedInt = myNum * myMulti;
        gameMan.MakeNumber();
    }

    void Start()
    {
        myNum = 0;
        


        UpdateNum();


    }


   
}
