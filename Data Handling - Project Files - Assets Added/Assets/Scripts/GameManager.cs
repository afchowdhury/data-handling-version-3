﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    public GameObject changeToWrittenPVButton;
    public GameObject changeToNumberPVButton;

    private bool isWritten = false;

    public float number;

    public NumberValue[] numberValueArrays;
    public TMP_Text[] placeValueWrittenArrays;
    public TMP_Text[] placeValueNumbersArrays;

    private void Start()
    {

        PVButtonPressed();

    }

    public void PVButtonPressed()
    {
        isWritten = !isWritten;

        if (isWritten == false)
        {
            changeToNumberPVButton.SetActive(false);

            changeToWrittenPVButton.SetActive(true);

            TogglePVButtons();
        }
        else
        {
            changeToWrittenPVButton.SetActive(false);

            changeToNumberPVButton.SetActive(true);

            TogglePVButtons();
        }
    }


    private void TogglePVButtons()
    {
        for  (int i = 0; i < placeValueWrittenArrays.Length; i++)
        {
           placeValueWrittenArrays[i].enabled = isWritten;
           placeValueNumbersArrays[i].enabled = !isWritten;
        }
    }


    public void MakeNumber()
    {
        number = 0;
        for (int i = 0; i < numberValueArrays.Length; i++)
        {
            number += numberValueArrays[i].passedInt;
        }
        float temp = number;
        number = Mathf.Round(number * 1000.000f) /1000.000f;
        
    }


    public void Multiply(int incDec)
    {

        if (numberValueArrays[-1+incDec].myNum + numberValueArrays[0].myNum==0)
        {
            for (int i = 0; i < numberValueArrays.Length; i++)
            {

                int temp = i + incDec;
                if (temp < numberValueArrays.Length)
                {
                    numberValueArrays[i].myNum = numberValueArrays[i + incDec].myNum;
                }

                else
                {
                    numberValueArrays[i].myNum = 0;
                }
                numberValueArrays[i].UpdateNum();

            }
        }

    }
    

   

    public void Divide(int incDec)
    {
        if (numberValueArrays[numberValueArrays.Length - incDec].myNum + numberValueArrays[numberValueArrays.Length-1].myNum == 0)

            for (int i = numberValueArrays.Length-1; i > -1; i--)
        {
            int temp = i - incDec;
            //Debug.Log(temp.ToString());
            if (temp > -1)
            {
                numberValueArrays[i].myNum = numberValueArrays[i - incDec].myNum;
            }

            else
            {
                numberValueArrays[i].myNum = 0;
            }

            numberValueArrays[i].UpdateNum();

        }

    }


}
